# weather-in-my-city

Crie um aplicativo da web que permitirá que os usuários pesquisem as condições atuais do tempo
usando o nome de uma cidade. Você pode usar a API Open Weather Map para obter os dados meteorológicos

# Uso

```sh
ng serve --host 0.0.0.0 --port 8080
```

Acesse http://localhost:8080 e informe a cidade, país, exemplo brasilia,br
