import { Component } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'weatherInMyCity';

  key: string = 'bd6cfe07f99d75d6d7759dc2cac4961c';
  cidade: string = '';
  tempo = [];
  city = {};

  constructor(private http: HttpClient) { }

  findCidade() {
    this.city = {};
    this.tempo = [];
    this.http.get(`http://api.openweathermap.org/data/2.5/forecast?q=${this.cidade}&appid=${this.key}`, {
      observe: 'response'
    }).subscribe((s: any) => {

      s.body.list.forEach(l => {
        l.weather.forEach(t => {
          this.tempo.push({
            main: t.main,
            description: t.description,
            dt: new Date(l.dt * 1000)
          })
        });
      });

      console.log(this.tempo);
      this.city = s.body.city;
    });
  }

}

